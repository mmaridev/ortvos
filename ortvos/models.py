# Copyright (c) 2018-2019 Marco Marinello <mmarinello@sezf.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Sitting(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("name"))

    admin = models.ForeignKey("auth.user", on_delete=models.PROTECT, verbose_name=_("administrator"))


    class Meta:
        verbose_name = _("Sitting")
        verbose_name_plural = _("Sittings")
        ordering = ["name"]


class SittingUser(models.Model):
    sitting = models.ForeignKey(Sitting, on_delete=models.CASCADE)

    last_name = models.CharField(max_length=200, verbose_name=_("last name"))

    first_name = models.CharField(max_length=200, verbose_name=_("first name"))


    class Meta:
        verbose_name = _("Sitting user")
        verbose_name_plural = _("Sitting users")
        ordering = ["sitting", "last_name", "first_name"]


class Votation(models.Model):
    sitting = models.ForeignKey(Sitting, on_delete=models.CASCADE, verbose_name=_("sitting"))

    name = models.CharField(max_length=200, verbose_name=_("name"))

    states = [
        ["queued", _("Queued")],
        ["voting", _("Now voting")],
        ["closed", _("Closed")]
    ]

    status = models.CharField(max_length=10, choices=states, verbose_name=_("status"))


    class Meta:
        verbose_name = _("Votation")
        verbose_name_plural = _("Votations")
        ordering = ["sitting"]


class VotationVote(models.Model):
    votation = models.ForeignKey(VotationSession, on_delete=models.CASCADE, verbose_name=_("votation"))

    user = models.ForeignKey(SittingUser, on_delete=models.CASCADE, verbose_name=_("user"))

    votes = [
        ["favourable", _("Favourable")],
        ["contrary", _("Contrary")],
        ["abstaining", _("Abstaining")]
    ]

    vote = models.CharField(max_length=20, choices=votes, verbose_name=_("vote"))


    class Meta:
        verbose_name = _("votation")
        verbose_name_plural = _("votations")
        ordering = ["votation", "user"]
